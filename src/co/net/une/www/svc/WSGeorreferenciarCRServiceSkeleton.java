/**
 * WSGeorreferenciarCRServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
package co.net.une.www.svc;

/**
 * WSGeorreferenciarCRServiceSkeleton java skeleton for the axisService
 */
public class WSGeorreferenciarCRServiceSkeleton {

	/**
	 * Auto generated method signature
	 * 
	 * @param wSGeorreferenciarCRRQ
	 */

	public co.net.une.www.gis.WSGeorreferenciarCRRS georeferenciarCR(
			co.net.une.www.gis.WSGeorreferenciarCRRQ wSGeorreferenciarCRRQ) {
		// TODO : fill this with the necessary business logic
		throw new java.lang.UnsupportedOperationException("Please implement "
				+ this.getClass().getName() + "#georeferenciarCR");
	}

}
