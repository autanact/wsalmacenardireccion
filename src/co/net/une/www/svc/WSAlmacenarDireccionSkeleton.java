
/**
 * WSAlmacenarDireccionSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
    package co.net.une.www.svc;
    /**
     *  WSAlmacenarDireccionSkeleton java skeleton for the axisService
     */
    public class WSAlmacenarDireccionSkeleton{
        
         
        /**
         * Auto generated method signature
         * 
                                     * @param wSAlmacenarDireccionRQ
         */
        
                 public co.net.une.www.gis.WSAlmacenarDireccionRS respuestaDireccionExcepcionada
                  (
                  co.net.une.www.gis.WSAlmacenarDireccionRQ wSAlmacenarDireccionRQ
                  )
            {
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#respuestaDireccionExcepcionada");
        }
     
    }
    