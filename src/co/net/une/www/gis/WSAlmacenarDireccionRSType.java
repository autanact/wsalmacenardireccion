
/**
 * WSAlmacenarDireccionRSType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package co.net.une.www.gis;
            

            /**
            *  WSAlmacenarDireccionRSType bean class
            */
        
        public  class WSAlmacenarDireccionRSType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = WSAlmacenarDireccion-RS-Type
                Namespace URI = http://www.une.net.co/gis
                Namespace Prefix = ns1
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://www.une.net.co/gis")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for DireccionNatural
                        */

                        
                                    protected co.net.une.www.gis.BoundedString250 localDireccionNatural ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString250
                           */
                           public  co.net.une.www.gis.BoundedString250 getDireccionNatural(){
                               return localDireccionNatural;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DireccionNatural
                               */
                               public void setDireccionNatural(co.net.une.www.gis.BoundedString250 param){
                            
                                            this.localDireccionNatural=param;
                                    

                               }
                            

                        /**
                        * field for CodigoDireccion
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localCodigoDireccion ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getCodigoDireccion(){
                               return localCodigoDireccion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoDireccion
                               */
                               public void setCodigoDireccion(co.net.une.www.gis.BoundedString100 param){
                            
                                            this.localCodigoDireccion=param;
                                    

                               }
                            

                        /**
                        * field for CodigoPais
                        */

                        
                                    protected co.net.une.www.gis.BoundedString2 localCodigoPais ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString2
                           */
                           public  co.net.une.www.gis.BoundedString2 getCodigoPais(){
                               return localCodigoPais;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoPais
                               */
                               public void setCodigoPais(co.net.une.www.gis.BoundedString2 param){
                            
                                            this.localCodigoPais=param;
                                    

                               }
                            

                        /**
                        * field for CodigoDepartamento
                        */

                        
                                    protected co.net.une.www.gis.BoundedString2 localCodigoDepartamento ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString2
                           */
                           public  co.net.une.www.gis.BoundedString2 getCodigoDepartamento(){
                               return localCodigoDepartamento;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoDepartamento
                               */
                               public void setCodigoDepartamento(co.net.une.www.gis.BoundedString2 param){
                            
                                            this.localCodigoDepartamento=param;
                                    

                               }
                            

                        /**
                        * field for CodigoMunicipio
                        */

                        
                                    protected co.net.une.www.gis.BoundedString8 localCodigoMunicipio ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString8
                           */
                           public  co.net.une.www.gis.BoundedString8 getCodigoMunicipio(){
                               return localCodigoMunicipio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoMunicipio
                               */
                               public void setCodigoMunicipio(co.net.une.www.gis.BoundedString8 param){
                            
                                            this.localCodigoMunicipio=param;
                                    

                               }
                            

                        /**
                        * field for CodigoComuna
                        */

                        
                                    protected co.net.une.www.gis.BoundedString10 localCodigoComuna ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoComunaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString10
                           */
                           public  co.net.une.www.gis.BoundedString10 getCodigoComuna(){
                               return localCodigoComuna;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoComuna
                               */
                               public void setCodigoComuna(co.net.une.www.gis.BoundedString10 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoComunaTracker = true;
                                       } else {
                                          localCodigoComunaTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoComuna=param;
                                    

                               }
                            

                        /**
                        * field for NombreComuna
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localNombreComuna ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNombreComunaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getNombreComuna(){
                               return localNombreComuna;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NombreComuna
                               */
                               public void setNombreComuna(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localNombreComunaTracker = true;
                                       } else {
                                          localNombreComunaTracker = false;
                                              
                                       }
                                   
                                            this.localNombreComuna=param;
                                    

                               }
                            

                        /**
                        * field for CodigoBarrio
                        */

                        
                                    protected co.net.une.www.gis.BoundedString10 localCodigoBarrio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoBarrioTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString10
                           */
                           public  co.net.une.www.gis.BoundedString10 getCodigoBarrio(){
                               return localCodigoBarrio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoBarrio
                               */
                               public void setCodigoBarrio(co.net.une.www.gis.BoundedString10 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoBarrioTracker = true;
                                       } else {
                                          localCodigoBarrioTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoBarrio=param;
                                    

                               }
                            

                        /**
                        * field for NombreBarrio
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localNombreBarrio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNombreBarrioTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getNombreBarrio(){
                               return localNombreBarrio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NombreBarrio
                               */
                               public void setNombreBarrio(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localNombreBarrioTracker = true;
                                       } else {
                                          localNombreBarrioTracker = false;
                                              
                                       }
                                   
                                            this.localNombreBarrio=param;
                                    

                               }
                            

                        /**
                        * field for CodigoManzana
                        */

                        
                                    protected co.net.une.www.gis.BoundedString14 localCodigoManzana ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoManzanaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString14
                           */
                           public  co.net.une.www.gis.BoundedString14 getCodigoManzana(){
                               return localCodigoManzana;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoManzana
                               */
                               public void setCodigoManzana(co.net.une.www.gis.BoundedString14 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoManzanaTracker = true;
                                       } else {
                                          localCodigoManzanaTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoManzana=param;
                                    

                               }
                            

                        /**
                        * field for CodigoPredio
                        */

                        
                                    protected co.net.une.www.gis.BoundedString17 localCodigoPredio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoPredioTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString17
                           */
                           public  co.net.une.www.gis.BoundedString17 getCodigoPredio(){
                               return localCodigoPredio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoPredio
                               */
                               public void setCodigoPredio(co.net.une.www.gis.BoundedString17 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoPredioTracker = true;
                                       } else {
                                          localCodigoPredioTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoPredio=param;
                                    

                               }
                            

                        /**
                        * field for DireccionNormalizada
                        */

                        
                                    protected co.net.une.www.gis.BoundedString250 localDireccionNormalizada ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString250
                           */
                           public  co.net.une.www.gis.BoundedString250 getDireccionNormalizada(){
                               return localDireccionNormalizada;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DireccionNormalizada
                               */
                               public void setDireccionNormalizada(co.net.une.www.gis.BoundedString250 param){
                            
                                            this.localDireccionNormalizada=param;
                                    

                               }
                            

                        /**
                        * field for Rural
                        */

                        
                                    protected co.net.une.www.gis.BoundedString2 localRural ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString2
                           */
                           public  co.net.une.www.gis.BoundedString2 getRural(){
                               return localRural;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rural
                               */
                               public void setRural(co.net.une.www.gis.BoundedString2 param){
                            
                                            this.localRural=param;
                                    

                               }
                            

                        /**
                        * field for Placa
                        */

                        
                                    protected co.net.une.www.gis.BoundedString10 localPlaca ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPlacaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString10
                           */
                           public  co.net.une.www.gis.BoundedString10 getPlaca(){
                               return localPlaca;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Placa
                               */
                               public void setPlaca(co.net.une.www.gis.BoundedString10 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localPlacaTracker = true;
                                       } else {
                                          localPlacaTracker = false;
                                              
                                       }
                                   
                                            this.localPlaca=param;
                                    

                               }
                            

                        /**
                        * field for Agregado
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localAgregado ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAgregadoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getAgregado(){
                               return localAgregado;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Agregado
                               */
                               public void setAgregado(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localAgregadoTracker = true;
                                       } else {
                                          localAgregadoTracker = false;
                                              
                                       }
                                   
                                            this.localAgregado=param;
                                    

                               }
                            

                        /**
                        * field for Remanente
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localRemanente ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRemanenteTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getRemanente(){
                               return localRemanente;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Remanente
                               */
                               public void setRemanente(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localRemanenteTracker = true;
                                       } else {
                                          localRemanenteTracker = false;
                                              
                                       }
                                   
                                            this.localRemanente=param;
                                    

                               }
                            

                        /**
                        * field for DireccionProveedor
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localDireccionProveedor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDireccionProveedorTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getDireccionProveedor(){
                               return localDireccionProveedor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DireccionProveedor
                               */
                               public void setDireccionProveedor(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDireccionProveedorTracker = true;
                                       } else {
                                          localDireccionProveedorTracker = false;
                                              
                                       }
                                   
                                            this.localDireccionProveedor=param;
                                    

                               }
                            

                        /**
                        * field for Estrato
                        */

                        
                                    protected int localEstrato ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstratoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getEstrato(){
                               return localEstrato;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Estrato
                               */
                               public void setEstrato(int param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Integer.MIN_VALUE) {
                                           localEstratoTracker = false;
                                              
                                       } else {
                                          localEstratoTracker = true;
                                       }
                                   
                                            this.localEstrato=param;
                                    

                               }
                            

                        /**
                        * field for Latitud
                        */

                        
                                    protected float localLatitud ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLatitudTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return float
                           */
                           public  float getLatitud(){
                               return localLatitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Latitud
                               */
                               public void setLatitud(float param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (java.lang.Float.isNaN(param)) {
                                           localLatitudTracker = false;
                                              
                                       } else {
                                          localLatitudTracker = true;
                                       }
                                   
                                            this.localLatitud=param;
                                    

                               }
                            

                        /**
                        * field for Longitud
                        */

                        
                                    protected float localLongitud ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLongitudTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return float
                           */
                           public  float getLongitud(){
                               return localLongitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Longitud
                               */
                               public void setLongitud(float param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (java.lang.Float.isNaN(param)) {
                                           localLongitudTracker = false;
                                              
                                       } else {
                                          localLongitudTracker = true;
                                       }
                                   
                                            this.localLongitud=param;
                                    

                               }
                            

                        /**
                        * field for EstadoGeoreferenciacion
                        */

                        
                                    protected co.net.une.www.gis.BoundedString1 localEstadoGeoreferenciacion ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString1
                           */
                           public  co.net.une.www.gis.BoundedString1 getEstadoGeoreferenciacion(){
                               return localEstadoGeoreferenciacion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstadoGeoreferenciacion
                               */
                               public void setEstadoGeoreferenciacion(co.net.une.www.gis.BoundedString1 param){
                            
                                            this.localEstadoGeoreferenciacion=param;
                                    

                               }
                            

                        /**
                        * field for CodigoRespuesta
                        */

                        
                                    protected java.lang.String localCodigoRespuesta ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCodigoRespuesta(){
                               return localCodigoRespuesta;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoRespuesta
                               */
                               public void setCodigoRespuesta(java.lang.String param){
                            
                                            this.localCodigoRespuesta=param;
                                    

                               }
                            

                        /**
                        * field for FechaRespuesta
                        */

                        
                                    protected co.net.une.www.gis.BoundedString19 localFechaRespuesta ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString19
                           */
                           public  co.net.une.www.gis.BoundedString19 getFechaRespuesta(){
                               return localFechaRespuesta;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FechaRespuesta
                               */
                               public void setFechaRespuesta(co.net.une.www.gis.BoundedString19 param){
                            
                                            this.localFechaRespuesta=param;
                                    

                               }
                            

                        /**
                        * field for CodigoError
                        */

                        
                                    protected java.lang.String localCodigoError ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoErrorTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCodigoError(){
                               return localCodigoError;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoError
                               */
                               public void setCodigoError(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoErrorTracker = true;
                                       } else {
                                          localCodigoErrorTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoError=param;
                                    

                               }
                            

                        /**
                        * field for DescripcionError
                        */

                        
                                    protected java.lang.String localDescripcionError ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescripcionErrorTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescripcionError(){
                               return localDescripcionError;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DescripcionError
                               */
                               public void setDescripcionError(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDescripcionErrorTracker = true;
                                       } else {
                                          localDescripcionErrorTracker = false;
                                              
                                       }
                                   
                                            this.localDescripcionError=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       WSAlmacenarDireccionRSType.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://www.une.net.co/gis");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":WSAlmacenarDireccion-RS-Type",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "WSAlmacenarDireccion-RS-Type",
                           xmlWriter);
                   }

               
                   }
               
                                            if (localDireccionNatural==null){
                                                 throw new org.apache.axis2.databinding.ADBException("DireccionNatural cannot be null!!");
                                            }
                                           localDireccionNatural.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","DireccionNatural"),
                                               factory,xmlWriter);
                                        
                                            if (localCodigoDireccion==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoDireccion cannot be null!!");
                                            }
                                           localCodigoDireccion.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoDireccion"),
                                               factory,xmlWriter);
                                        
                                            if (localCodigoPais==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoPais cannot be null!!");
                                            }
                                           localCodigoPais.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoPais"),
                                               factory,xmlWriter);
                                        
                                            if (localCodigoDepartamento==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoDepartamento cannot be null!!");
                                            }
                                           localCodigoDepartamento.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoDepartamento"),
                                               factory,xmlWriter);
                                        
                                            if (localCodigoMunicipio==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoMunicipio cannot be null!!");
                                            }
                                           localCodigoMunicipio.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoMunicipio"),
                                               factory,xmlWriter);
                                         if (localCodigoComunaTracker){
                                            if (localCodigoComuna==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoComuna cannot be null!!");
                                            }
                                           localCodigoComuna.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoComuna"),
                                               factory,xmlWriter);
                                        } if (localNombreComunaTracker){
                                            if (localNombreComuna==null){
                                                 throw new org.apache.axis2.databinding.ADBException("NombreComuna cannot be null!!");
                                            }
                                           localNombreComuna.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","NombreComuna"),
                                               factory,xmlWriter);
                                        } if (localCodigoBarrioTracker){
                                            if (localCodigoBarrio==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoBarrio cannot be null!!");
                                            }
                                           localCodigoBarrio.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoBarrio"),
                                               factory,xmlWriter);
                                        } if (localNombreBarrioTracker){
                                            if (localNombreBarrio==null){
                                                 throw new org.apache.axis2.databinding.ADBException("NombreBarrio cannot be null!!");
                                            }
                                           localNombreBarrio.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","NombreBarrio"),
                                               factory,xmlWriter);
                                        } if (localCodigoManzanaTracker){
                                            if (localCodigoManzana==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoManzana cannot be null!!");
                                            }
                                           localCodigoManzana.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoManzana"),
                                               factory,xmlWriter);
                                        } if (localCodigoPredioTracker){
                                            if (localCodigoPredio==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoPredio cannot be null!!");
                                            }
                                           localCodigoPredio.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoPredio"),
                                               factory,xmlWriter);
                                        }
                                            if (localDireccionNormalizada==null){
                                                 throw new org.apache.axis2.databinding.ADBException("DireccionNormalizada cannot be null!!");
                                            }
                                           localDireccionNormalizada.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","DireccionNormalizada"),
                                               factory,xmlWriter);
                                        
                                            if (localRural==null){
                                                 throw new org.apache.axis2.databinding.ADBException("Rural cannot be null!!");
                                            }
                                           localRural.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","Rural"),
                                               factory,xmlWriter);
                                         if (localPlacaTracker){
                                            if (localPlaca==null){
                                                 throw new org.apache.axis2.databinding.ADBException("Placa cannot be null!!");
                                            }
                                           localPlaca.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","Placa"),
                                               factory,xmlWriter);
                                        } if (localAgregadoTracker){
                                            if (localAgregado==null){
                                                 throw new org.apache.axis2.databinding.ADBException("Agregado cannot be null!!");
                                            }
                                           localAgregado.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","Agregado"),
                                               factory,xmlWriter);
                                        } if (localRemanenteTracker){
                                            if (localRemanente==null){
                                                 throw new org.apache.axis2.databinding.ADBException("Remanente cannot be null!!");
                                            }
                                           localRemanente.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","Remanente"),
                                               factory,xmlWriter);
                                        } if (localDireccionProveedorTracker){
                                            if (localDireccionProveedor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("DireccionProveedor cannot be null!!");
                                            }
                                           localDireccionProveedor.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","DireccionProveedor"),
                                               factory,xmlWriter);
                                        } if (localEstratoTracker){
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Estrato", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Estrato");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Estrato");
                                    }
                                
                                               if (localEstrato==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Estrato cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstrato));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLatitudTracker){
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Latitud", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Latitud");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Latitud");
                                    }
                                
                                               if (java.lang.Float.isNaN(localLatitud)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Latitud cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLatitud));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLongitudTracker){
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Longitud", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Longitud");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Longitud");
                                    }
                                
                                               if (java.lang.Float.isNaN(localLongitud)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Longitud cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLongitud));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             }
                                            if (localEstadoGeoreferenciacion==null){
                                                 throw new org.apache.axis2.databinding.ADBException("EstadoGeoreferenciacion cannot be null!!");
                                            }
                                           localEstadoGeoreferenciacion.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","EstadoGeoreferenciacion"),
                                               factory,xmlWriter);
                                        
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"CodigoRespuesta", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"CodigoRespuesta");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("CodigoRespuesta");
                                    }
                                

                                          if (localCodigoRespuesta==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("CodigoRespuesta cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCodigoRespuesta);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                            if (localFechaRespuesta==null){
                                                 throw new org.apache.axis2.databinding.ADBException("FechaRespuesta cannot be null!!");
                                            }
                                           localFechaRespuesta.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","FechaRespuesta"),
                                               factory,xmlWriter);
                                         if (localCodigoErrorTracker){
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"CodigoError", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"CodigoError");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("CodigoError");
                                    }
                                

                                          if (localCodigoError==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("CodigoError cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCodigoError);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDescripcionErrorTracker){
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"DescripcionError", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"DescripcionError");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("DescripcionError");
                                    }
                                

                                          if (localDescripcionError==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("DescripcionError cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescripcionError);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "DireccionNatural"));
                            
                            
                                    if (localDireccionNatural==null){
                                         throw new org.apache.axis2.databinding.ADBException("DireccionNatural cannot be null!!");
                                    }
                                    elementList.add(localDireccionNatural);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoDireccion"));
                            
                            
                                    if (localCodigoDireccion==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoDireccion cannot be null!!");
                                    }
                                    elementList.add(localCodigoDireccion);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoPais"));
                            
                            
                                    if (localCodigoPais==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoPais cannot be null!!");
                                    }
                                    elementList.add(localCodigoPais);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoDepartamento"));
                            
                            
                                    if (localCodigoDepartamento==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoDepartamento cannot be null!!");
                                    }
                                    elementList.add(localCodigoDepartamento);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoMunicipio"));
                            
                            
                                    if (localCodigoMunicipio==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoMunicipio cannot be null!!");
                                    }
                                    elementList.add(localCodigoMunicipio);
                                 if (localCodigoComunaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoComuna"));
                            
                            
                                    if (localCodigoComuna==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoComuna cannot be null!!");
                                    }
                                    elementList.add(localCodigoComuna);
                                } if (localNombreComunaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "NombreComuna"));
                            
                            
                                    if (localNombreComuna==null){
                                         throw new org.apache.axis2.databinding.ADBException("NombreComuna cannot be null!!");
                                    }
                                    elementList.add(localNombreComuna);
                                } if (localCodigoBarrioTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoBarrio"));
                            
                            
                                    if (localCodigoBarrio==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoBarrio cannot be null!!");
                                    }
                                    elementList.add(localCodigoBarrio);
                                } if (localNombreBarrioTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "NombreBarrio"));
                            
                            
                                    if (localNombreBarrio==null){
                                         throw new org.apache.axis2.databinding.ADBException("NombreBarrio cannot be null!!");
                                    }
                                    elementList.add(localNombreBarrio);
                                } if (localCodigoManzanaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoManzana"));
                            
                            
                                    if (localCodigoManzana==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoManzana cannot be null!!");
                                    }
                                    elementList.add(localCodigoManzana);
                                } if (localCodigoPredioTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoPredio"));
                            
                            
                                    if (localCodigoPredio==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoPredio cannot be null!!");
                                    }
                                    elementList.add(localCodigoPredio);
                                }
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "DireccionNormalizada"));
                            
                            
                                    if (localDireccionNormalizada==null){
                                         throw new org.apache.axis2.databinding.ADBException("DireccionNormalizada cannot be null!!");
                                    }
                                    elementList.add(localDireccionNormalizada);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "Rural"));
                            
                            
                                    if (localRural==null){
                                         throw new org.apache.axis2.databinding.ADBException("Rural cannot be null!!");
                                    }
                                    elementList.add(localRural);
                                 if (localPlacaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "Placa"));
                            
                            
                                    if (localPlaca==null){
                                         throw new org.apache.axis2.databinding.ADBException("Placa cannot be null!!");
                                    }
                                    elementList.add(localPlaca);
                                } if (localAgregadoTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "Agregado"));
                            
                            
                                    if (localAgregado==null){
                                         throw new org.apache.axis2.databinding.ADBException("Agregado cannot be null!!");
                                    }
                                    elementList.add(localAgregado);
                                } if (localRemanenteTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "Remanente"));
                            
                            
                                    if (localRemanente==null){
                                         throw new org.apache.axis2.databinding.ADBException("Remanente cannot be null!!");
                                    }
                                    elementList.add(localRemanente);
                                } if (localDireccionProveedorTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "DireccionProveedor"));
                            
                            
                                    if (localDireccionProveedor==null){
                                         throw new org.apache.axis2.databinding.ADBException("DireccionProveedor cannot be null!!");
                                    }
                                    elementList.add(localDireccionProveedor);
                                } if (localEstratoTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "Estrato"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstrato));
                            } if (localLatitudTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "Latitud"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLatitud));
                            } if (localLongitudTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "Longitud"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLongitud));
                            }
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "EstadoGeoreferenciacion"));
                            
                            
                                    if (localEstadoGeoreferenciacion==null){
                                         throw new org.apache.axis2.databinding.ADBException("EstadoGeoreferenciacion cannot be null!!");
                                    }
                                    elementList.add(localEstadoGeoreferenciacion);
                                
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoRespuesta"));
                                 
                                        if (localCodigoRespuesta != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCodigoRespuesta));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("CodigoRespuesta cannot be null!!");
                                        }
                                    
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "FechaRespuesta"));
                            
                            
                                    if (localFechaRespuesta==null){
                                         throw new org.apache.axis2.databinding.ADBException("FechaRespuesta cannot be null!!");
                                    }
                                    elementList.add(localFechaRespuesta);
                                 if (localCodigoErrorTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoError"));
                                 
                                        if (localCodigoError != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCodigoError));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("CodigoError cannot be null!!");
                                        }
                                    } if (localDescripcionErrorTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "DescripcionError"));
                                 
                                        if (localDescripcionError != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescripcionError));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("DescripcionError cannot be null!!");
                                        }
                                    }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static WSAlmacenarDireccionRSType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            WSAlmacenarDireccionRSType object =
                new WSAlmacenarDireccionRSType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"WSAlmacenarDireccion-RS-Type".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WSAlmacenarDireccionRSType)co.net.une.www.gis.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","DireccionNatural").equals(reader.getName())){
                                
                                                object.setDireccionNatural(co.net.une.www.gis.BoundedString250.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoDireccion").equals(reader.getName())){
                                
                                                object.setCodigoDireccion(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoPais").equals(reader.getName())){
                                
                                                object.setCodigoPais(co.net.une.www.gis.BoundedString2.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoDepartamento").equals(reader.getName())){
                                
                                                object.setCodigoDepartamento(co.net.une.www.gis.BoundedString2.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoMunicipio").equals(reader.getName())){
                                
                                                object.setCodigoMunicipio(co.net.une.www.gis.BoundedString8.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoComuna").equals(reader.getName())){
                                
                                                object.setCodigoComuna(co.net.une.www.gis.BoundedString10.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","NombreComuna").equals(reader.getName())){
                                
                                                object.setNombreComuna(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoBarrio").equals(reader.getName())){
                                
                                                object.setCodigoBarrio(co.net.une.www.gis.BoundedString10.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","NombreBarrio").equals(reader.getName())){
                                
                                                object.setNombreBarrio(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoManzana").equals(reader.getName())){
                                
                                                object.setCodigoManzana(co.net.une.www.gis.BoundedString14.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoPredio").equals(reader.getName())){
                                
                                                object.setCodigoPredio(co.net.une.www.gis.BoundedString17.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","DireccionNormalizada").equals(reader.getName())){
                                
                                                object.setDireccionNormalizada(co.net.une.www.gis.BoundedString250.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","Rural").equals(reader.getName())){
                                
                                                object.setRural(co.net.une.www.gis.BoundedString2.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","Placa").equals(reader.getName())){
                                
                                                object.setPlaca(co.net.une.www.gis.BoundedString10.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","Agregado").equals(reader.getName())){
                                
                                                object.setAgregado(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","Remanente").equals(reader.getName())){
                                
                                                object.setRemanente(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","DireccionProveedor").equals(reader.getName())){
                                
                                                object.setDireccionProveedor(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","Estrato").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstrato(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setEstrato(java.lang.Integer.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","Latitud").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLatitud(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToFloat(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLatitud(java.lang.Float.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","Longitud").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLongitud(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToFloat(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLongitud(java.lang.Float.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","EstadoGeoreferenciacion").equals(reader.getName())){
                                
                                                object.setEstadoGeoreferenciacion(co.net.une.www.gis.BoundedString1.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoRespuesta").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCodigoRespuesta(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","FechaRespuesta").equals(reader.getName())){
                                
                                                object.setFechaRespuesta(co.net.une.www.gis.BoundedString19.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoError").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCodigoError(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","DescripcionError").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescripcionError(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          